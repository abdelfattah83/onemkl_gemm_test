m=100
n=300
k_start=510
k_stop=515
k_step=1
verbose=v

for p in s d c z
do
    for transA in n t c
    do
        for transB in n t c
        do
            for((k=${k_start};k<=${k_stop};k=k+${k_step}))
            do
                ./gemm_test $p $m $n $k $transA $transB $verbose
                verbose=n
            done
            echo ""
        done
    done
    echo ""
done
