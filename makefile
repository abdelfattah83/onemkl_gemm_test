
# choose g++ or dpcpp
MYCC=icx

FLAGS=-DMKL_ILP64
INC=--std=c++17 -I${CMPROOT}/linux/include/sycl -I${MKLROOT}/include


# uncomment to use batch gemm
#FLAGS+=-DUSE_GEMM_BATCH

# uncomment to use shared mallocs
#FLAGS+=-DUSE_MALLOC_SHARED

ifeq ($(MYCC),g++)
	FLAGS+=-fpermissive
endif


LIBDIR=-L${MKLROOT}/lib/intel64 -L${CMPROOT}/linux/lib
LIB=-lmkl_sycl -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lsycl -lstdc++ -lpthread -lm -ldl

all:
	${MYCC} ${FLAGS} ${INC} -c -o gemm_test.o gemm_test.cpp
	${MYCC}	gemm_test.o -o gemm_test ${LIBDIR} ${LIB}

clean:
	rm -f gemm_test.o gemm_test
