#ifdef __GNUC__
#include <CL/sycl/detail/cl.h>
#endif

#include <CL/sycl.hpp>
#include <exception>
#include <iostream>
#include <oneapi/mkl.hpp>

using namespace sycl;
using namespace oneapi;

//-----------------------------------------------------------------------------
template<typename T>
T my_rand()
{
    return (T)std::rand() / (T)RAND_MAX;
}

template<>
std::complex<float> my_rand<std::complex<float>>()
{
    std::complex<float> x( (float)std::rand() / (float)RAND_MAX,
                           (float)std::rand() / (float)RAND_MAX );
    return x;
}

template<>
std::complex<double> my_rand<std::complex<double>>()
{
    std::complex<double> x( (double)std::rand() / (double)RAND_MAX,
                            (double)std::rand() / (double)RAND_MAX );
    return x;
}

//-----------------------------------------------------------------------------
mkl::transpose trans_lapack2mkl(char trans)
{
    switch (trans) {
        case 'n': return mkl::transpose::N; break;
        case 't': return mkl::transpose::T; break;
        case 'c': return mkl::transpose::C; break;
        default: return mkl::transpose::N;
    }
}

//-----------------------------------------------------------------------------
char trans_mkl2lapack(mkl::transpose trans)
{
    switch (trans) {
        case mkl::transpose::N: return 'n'; break;
        case mkl::transpose::T: return 't'; break;
        case mkl::transpose::C: return 'c'; break;
        default: return 'u'; // undefined
    }
}

//-----------------------------------------------------------------------------
void show_platforms() {
    auto platforms = platform::get_platforms();
    std::cout << "\x1b[34m ########## Platform(s) Information ##########\x1b[0m" << std::endl;
    for (auto &platform : platforms) {
        std::cout << " ==> INFO: Platform: "
	        << platform.get_info<info::platform::name>()
	        << std::endl;

        auto devices = platform.get_devices();
        printf(" ==> INFO: Number of platform devices = %lld\n", (long long)devices.size());
        for (auto &device : devices ) {
            std::cout << " ==> INFO: Device: "
		        << device.get_info<info::device::vendor>()
		        << "ID = " << (device.is_host() ? 0 : device.get())
		        << device.get_info<info::device::name>()
		        << std::endl;
        }
  }
  std::cout << "\x1b[34m ########## End Platform(s) Information ##########\x1b[0m"
            << std::endl << std::endl << std::endl;
}

//-----------------------------------------------------------------------------
template<typename T>
T* my_device_malloc(sycl::queue &queue, size_t size)
{
    T *dptr = NULL;
    const char* func = __func__;
    try{
        #ifdef USE_MALLOC_SHARED
        dptr = (T*)sycl::malloc_shared(size*sizeof(T), queue);
        #else
        dptr = (T*)sycl::malloc_device(size*sizeof(T), queue);
        #endif
    } catch (sycl::exception const& e) {
        std::cout << "\t\tCaught exception during "
        << __func__ << std::endl
        << e.what() << std::endl;
    }
    return dptr;
}

//-----------------------------------------------------------------------------
template<typename T>
void gemm_test( sycl::device device_gpu, char precision,
                mkl::transpose transA, mkl::transpose transB,
                int64_t m, int64_t n, int64_t k, T alpha, T beta,
                double tol)
{
    // hA, hB, hC
    int64_t lda = transA == mkl::transpose::N ? m : k;
    int64_t ldb = transB == mkl::transpose::N ? k : n;
    int64_t ldc = m;

    std::vector<T> hA(m*k);
    std::vector<T> hB(k*n);
    std::vector<T> hC(m*n);
    std::vector<T> hCdev(m*n);

    for(int i = 0; i < m*k; i++) hA[i] = my_rand<T>();
    for(int i = 0; i < k*n; i++) hB[i] = my_rand<T>();
    for(int i = 0; i < m*n; i++) hC[i] = my_rand<T>();

    memcpy(hCdev.data(), hC.data(), hC.size() * sizeof(T));

    // cpu gemm (reference output)
    sycl::device device_cpu;
    sycl::queue queue_cpu( device_cpu );
    oneapi::mkl::blas::gemm(
        queue_cpu,
        transA, transB,
        m, n, k,
        alpha, hA.data(), lda,
               hB.data(), ldb,
        beta,  hC.data(), ldc);
    queue_cpu.wait();

    // gpu gemm
    sycl::queue queue_gpu( device_gpu );
    T *dA, *dB, *dC;
    dA = my_device_malloc<T>(queue_gpu, hA.size());
    dB = my_device_malloc<T>(queue_gpu, hB.size());
    dC = my_device_malloc<T>(queue_gpu, hC.size());

    queue_gpu.memcpy(dA, hA.data(), hA.size()*sizeof(T));
    queue_gpu.memcpy(dB, hB.data(), hB.size()*sizeof(T));
    queue_gpu.memcpy(dC, hCdev.data(), hCdev.size()*sizeof(T));
    queue_gpu.wait();

    try {
        #ifndef USE_GEMM_BATCH
		    oneapi::mkl::blas::gemm(
		        queue_gpu,
	            transA, transB,
		        m, n, k,
		        alpha, dA, lda,
		               dB, ldb,
		        beta,  dC, ldc);

        #else
            int64_t batch_size = 1;
            std::vector<T*> hA_array(1, dA);
            std::vector<T*> hB_array(1, dB);
            std::vector<T*> hC_array(1, dC);
            T **dA_array, **dB_array, **dC_array;
            dA_array = my_device_malloc<T*>(queue_gpu, hA_array.size());
            dB_array = my_device_malloc<T*>(queue_gpu, hB_array.size());
            dC_array = my_device_malloc<T*>(queue_gpu, hC_array.size());
            queue_gpu.memcpy(dA_array, hA_array.data(), hA_array.size()*sizeof(T*));
            queue_gpu.memcpy(dB_array, hB_array.data(), hB_array.size()*sizeof(T*));
            queue_gpu.memcpy(dC_array, hC_array.data(), hC_array.size()*sizeof(T*));
            queue_gpu.wait();

            oneapi::mkl::blas::gemm_batch(
                queue_gpu,
                &transA, &transB,
	            &m, &n, &k, &alpha,
	            (const T**) dA_array, &lda,
	            (const T**) dB_array, &ldb,
	            &beta,
	            (T**) dC_array, &ldc, 1, &batch_size );
	            queue_gpu.wait();
	            sycl::free(dA_array, queue_gpu);
	            sycl::free(dB_array, queue_gpu);
	            sycl::free(dC_array, queue_gpu);
	    #endif
    }
    catch (sycl::exception const& e) {
        std::cout << "\t\tCaught synchronous SYCL exception during GEMM:\n"
            << e.what() << std::endl;
    }
    catch (std::exception const& e) {
        std::cout << "\t\tCaught synchronous STL exception during GEMM:\n"
            << e.what() << std::endl;
    }

    queue_gpu.wait();

    // Copy from host to device
    queue_gpu.memcpy(hCdev.data(), dC, hC.size()*sizeof(T));
    queue_gpu.wait();

    double max_relative_error = 0.;
    for(int i = 0; i < m*n; i++) {
            double err = (double) ( std::abs(hC[i] - hCdev[i]) / std::abs(hC[i]) );
	        max_relative_error = std::max(max_relative_error, err);
    }

    printf("\x1b[0m %9c   %6c   %6c   %5lld   %5lld   %5lld   %8.2e\x1b[0m",
            precision, trans_mkl2lapack(transA), trans_mkl2lapack(transB),
            (long long)m, (long long)n, (long long)k,
            max_relative_error );

    if( max_relative_error < tol )
        printf("\x1b[32m  PASS\x1b[0m\n");
    else
        printf("\x1b[31m  FAILED\x1b[0m\n");

    sycl::free(dA, queue_gpu);
    sycl::free(dB, queue_gpu);
    sycl::free(dC, queue_gpu);
}

int main(int argc, char *argv[]) {

    std::srand(time(NULL));

    // Create GPU device
    sycl::device device_gpu;
    try {
	    device_gpu = sycl::device(sycl::gpu_selector());
    }
    catch (...) {
        std::cout << "Warning: GPU device not found! Using default device instead." << std::endl;
    }

    // GEMM setup
    char precision = 's', tA = 'n', tB = 'n', verbose = 'v';
    int64_t m = 100, n = 300, k = 600;

    if(argc > 1) {
        precision = *argv[1];
    }

    // check precision
    if( !(precision == 's') && !(precision == 'd') &&
        !(precision == 'c') && !(precision == 'z') )
    {
        printf("\x1b[31m ==> ERROR: first argument must be one of (s, d, c, z)\x1b[0m\n");
        return 1;
    }

    if(argc > 2) {
        m = atoi(argv[2]);
    }

    if(argc > 3) {
        n = atoi(argv[3]);
    }

    if(argc > 4) {
        k = atoi(argv[4]);
    }

    if(argc > 5) {
        tA = *argv[5];
    }

    if( !(tA == 'n') && !(tA == 't') && !(tA == 'c') )
    {
        printf("\x1b[31m ==> ERROR: argument 5 (transA) must be one of (n, t, c)\x1b[0m\n");
        return 1;
    }

    if(argc > 6) {
        tB = *argv[6];
    }

    if( !(tB == 'n') && !(tB == 't') && !(tB == 'c') )
    {
        printf("\x1b[31m ==> ERROR: argument 6 (transB) must be one of (n, t, c)\x1b[0m\n");
        return 1;
    }

    if(argc > 7) {
        verbose = *argv[7];
    }

    if( verbose == 'v' ) {
        show_platforms();
        printf(" ==> INFO: selected GPU: ");
        std::cout << device_gpu.get_info<info::device::vendor>()
              << "  " << device_gpu.get_info<info::device::name>() << std::endl;

        printf("\x1b[35m Precision   transA   transB       M       N       K   Error\n");
    }

    mkl::transpose transA = trans_lapack2mkl(tA);
    mkl::transpose transB = trans_lapack2mkl(tB);

    if( precision == 's') {
        float alpha =  0.25;
        float beta  = -0.31;
        double tol   = 50. * (double)LAPACKE_slamch('e');
        gemm_test<float>(
            device_gpu, precision,
            transA, transB,
            m, n, k, alpha, beta, tol);
    }
    else if ( precision == 'd') {
        double alpha =  0.25;
        double beta  = -0.31;
        double tol   = 50. * LAPACKE_dlamch('e');
        gemm_test<double>(
            device_gpu, precision,
            transA, transB,
            m, n, k, alpha, beta, tol);
    }
    else if ( precision == 'c') {
        std::complex<float> alpha ( 0.25, 0.11);
        std::complex<float> beta  (-0.31, 0.22);
        double tol   = 50. * (double)LAPACKE_slamch('e');
        gemm_test<std::complex<float>>(
            device_gpu, precision,
            transA, transB,
            m, n, k, alpha, beta, tol);
    }
    else {
        std::complex<double> alpha ( 0.25, 0.11);
        std::complex<double> beta  (-0.31, 0.22);
        double tol   = 50. * LAPACKE_dlamch('e');
        gemm_test<std::complex<double>>(
            device_gpu, precision,
            transA, transB,
            m, n, k, alpha, beta, tol);
    }

    return 0;
}

